<?php include('includes/init.php'); ?>


<?php
$product = new Product;
$products = $product->get_products();

$basket = new Basket;
$basket_content = $basket->get_basket();

$hun_number_format = new NumberFormatter('hu_HU',  NumberFormatter::CURRENCY);


if ( !empty($basket_content) ) {

	// Calculate Discounts
	$discount = new Discount;


	// -- Discount 1 --

	$discount_1 = $discount->two_products_after_third_free_discount();
	$basket_with_discount_1 = $basket->get_basket();

	$discounts_1_sum_price = 0;
	$discounts_1_sum_quantity = 0;

	foreach ($basket_with_discount_1 as $product_id => $product_data) {
		if ( isset($basket_with_discount_1[$product_id]['discount_data']['price_in_huf']) ) {
			$discounts_1_sum_price += $basket_with_discount_1[$product_id]['discount_data']['price_in_huf'];
		}

		if ( isset($basket_with_discount_1[$product_id]['discount_data']['quantity']) ) {
			$discounts_1_sum_quantity += $basket_with_discount_1[$product_id]['discount_data']['quantity'];
		}
	}


	// -- Discount 2 --

	$discount_2 = $discount->three_products_are_the_same_discount();
	$basket_with_discount_2 = $basket->get_basket();

	$discounts_sum_price = 0;
	$discounts_sum_quantity = 0;

	foreach ($basket_with_discount_2 as $product_id => $product_data) {
		if ( isset($basket_with_discount_2[$product_id]['discount_data']['price_in_huf']) ) {
			$discounts_2_sum_price += $basket_with_discount_2[$product_id]['discount_data']['price_in_huf'];
		}

		if ( isset($basket_with_discount_2[$product_id]['discount_data']['quantity']) ) {
			$discounts_2_sum_quantity += $basket_with_discount_2[$product_id]['discount_data']['quantity'];
		}
	}


	// -- Discount 3 --

	$discount_3 = $discount->megapack_discount();
	$basket_with_discount_3 = $basket->get_basket();

	$discounts_sum_price = 0;
	$discounts_sum_quantity = 0;

	foreach ($basket_with_discount_3 as $product_id => $product_data) {
		if ( isset($basket_with_discount_3[$product_id]['discount_data']['price_in_huf']) ) {
			$discounts_3_sum_price += $basket_with_discount_3[$product_id]['discount_data']['price_in_huf'];
		}

		if ( isset($basket_with_discount_3[$product_id]['discount_data']['quantity']) ) {
			$discounts_3_sum_quantity += $basket_with_discount_3[$product_id]['discount_data']['quantity'];
		}
	}


	// --- Result ---

	// Most discounts

	$discounts_sum_prices = array(
									'discount1' => $discounts_1_sum_price,
									'discount2' => $discounts_2_sum_price,
									'discount3' => $discounts_3_sum_price,
								);

	arsort($discounts_sum_prices);
	foreach ($discounts_sum_prices as $key => $value) {
		$discounts_sum_prices = $key;
	}


	// Most free added products

	$discounts_sum_quantity = array(
									'discount1' => $discounts_1_sum_quantity,
									'discount2' => $discounts_2_sum_quantity,
									'discount3' => $discounts_3_sum_quantity,
								);

	arsort($discounts_sum_quantity);
	foreach ($discounts_sum_quantity as $key => $value) {
		$discounts_sum_quantity = $key;
	}


	if ( $discounts_sum_prices == 'discount1' ) 		{ $discount->two_products_after_third_free_discount(); }
	else if ( $discounts_sum_prices == 'discount2' ) 	{ $discount->three_products_are_the_same_discount(); }
	else if ( $discounts_sum_prices == 'discount3' ) 	{ $discount->megapack_discount(); }
}
?>


<?php
if ( isset($_POST['submit_basket']) ) {
	$basket->delete_basket();

	$selected_products = $_POST['product'];
	$selected_quantity = $_POST['quantity'];
	$selected_summary = array();

	foreach ($selected_products as $key => $product_id) {

		if ( intval($selected_quantity[$key]) > 0 ) {
			$selected_summary[] = array(
										'product_id' => (int) $product_id,
										'quantity' => (int) $selected_quantity[$key],
									);
		}
	}

	if ( !empty($selected_summary) ) {
		foreach ($selected_summary as $product_data) {
			$basket->set_product_to_basket($product_data);
		}
	}

	header( "Location: index.php" );
}


//echo "<pre>"; var_dump($basket->get_basket()); echo "</pre>";
?>


<!DOCTYPE html>
<html>
	<head>
		<title>Webshop</title>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

		<style type="text/css">
			.product_name { border: none; background: none; }
		</style>
	</head>
	<body>
		<div class="container">

			<?php if ( !empty($basket_content) ) { ?>
				<table class="table table-striped mt-5">
					<tr>
						<th colspan="7" class="text-center">Basket content</th>
					</tr>
					<tr>
						<th>Product name</th>
						<th>Quantity</th>
						<th>Original Price in HUF</th>
						<th>Original Price in EUR</th>
						<th>Discount Price</th>
						<th>Free Product</th>
						<th>Discount Type</th>
					</tr>
					<?php
					foreach ($basket_content as $product_data) {
						$p_id = $product_data['product_id'];
						$p_name = $product->get_product_attr_by_id('name', $p_id);
						$p_price_in_huf = $product_data['price_in_huf'];
						$p_price_in_eur = "...";
						$p_quantity = $product_data['quantity'];
						$discount_price = $discount->get_product_discount_price($p_id);

						if ( !empty($discount_price) ) {
							$discount_price = $hun_number_format->formatCurrency($discount_price, 'HUF');
						}


						$discount_type = $discount->get_product_discount_type($p_id);
						if ( $discount_type === FALSE ) {
							$discount_type = "";
						}

						?>
						<tr>
							<td><?php echo $p_name; ?></td>
							<td><?php echo $p_quantity; ?></td>
							<td><?php echo $hun_number_format->formatCurrency($p_price_in_huf, 'HUF'); ?></td>
							<td><?php echo $p_price_in_eur; ?></td>
							<td><?php echo $discount_price; ?></td>
							<td><?php echo $discount->get_product_discount_quantity($p_id); ?></td>
							<td><?php echo $discount_type; ?></td>
						</tr>
						<?php
					}
					?>
				</table>
				<br>
			<?php } ?>

			<form method="POST" action="" class="mt-5">

				<table class="table table-striped">
					<tr>
						<th colspan="2" class="text-center">Products</th>
					</tr>
					<tr>
						<th>Product name</th>
						<th>Quantity</th>
					</tr>
				<?php
				foreach ($products as $product_id => $product_data) {

					?>
					<tr>
						<td>
							<input class="product_name" type="text" name="" value="<?php echo $product_data['name']; ?>:" readonly>
							<input type="hidden" name="product[]" value="<?php echo $product_id; ?>">
						</td>
						<td>
							<input type="number" name="quantity[]" value="0">
						</td>
					</tr>
					<?php
				}
				?>
				</table>

				<br>
				<input type="submit" value="Submit" name="submit_basket" class="btn btn-primary">
			</form>	

		</div>
	</body>
</html>
