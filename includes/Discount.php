<?php 

class Discount extends Functions {


	// 2 product --> +1 free
	public function two_products_after_third_free_discount() {
		$basket = new Basket;
		$basket_content = $basket->get_basket();


		$discount_name = 'discount-1';
		$discount_price = 0;
		$discount_quantity = 0;


		if ( !empty($basket_content) && is_array($basket_content) ) {
			foreach ($basket_content as $product_data) {
				$p_id = $product_data['product_id'];
				$p_quantity = (int) $product_data['quantity'];

				if ( $p_quantity > 2 ) {
					$amount_of_free_products = (int) floor($p_quantity / 2);


					if ( $amount_of_free_products > 0 ) {
						$basket->change_basket_product_discount_price($p_id, $discount_price);
						$basket->change_basket_product_discount_quantity($p_id, $amount_of_free_products);
						$basket->change_basket_product_discount_name($p_id, $discount_name);
					}
				}
			}
		}
	}


	// 3 same product --> 1 free
	public function three_products_are_the_same_discount() {
		$basket = new Basket;
		$basket_content = $basket->get_basket();

		$product = new Product;

		$discount_name = 'discount-2';
		$discount_price = 0;
		$discount_quantity = 0;


		if ( !empty($basket_content) && is_array($basket_content) ) {
			foreach ($basket_content as $product_data) {
				$p_id = $product_data['product_id'];
				$p_all_price_in_huf = $product_data['price_in_huf'];
				$p_quantity = (int) $product_data['quantity'];

				$one_product_price_in_huf = $product->get_product_attr_by_id('price', $p_id);


				if ( ($p_quantity >= 3) && (intval($one_product_price_in_huf) > 0) ) {

					$number_of_amount_deduction_product = (int) floor($p_quantity / 3);


					if ( $number_of_amount_deduction_product > 0 ) {
						$discount_price = - ($number_of_amount_deduction_product * $one_product_price_in_huf);

						$basket->change_basket_product_discount_price($p_id, $discount_price);
						$basket->change_basket_product_discount_quantity($p_id, $discount_quantity);
						$basket->change_basket_product_discount_name($p_id, $discount_name);
					}
				}
			}
		}
	}


	// If product's megapack type is TRUE
	// 12 same prod --> -6000 Ft
	public function megapack_discount() {
		$basket = new Basket;
		$basket_content = $basket->get_basket();

		$product = new Product;

		$discount_name = 'discount-3';
		$discount_price = 0;
		$discount_quantity = 0;


		if ( !empty($basket_content) && is_array($basket_content) ) {
			foreach ($basket_content as $product_data) {
				$p_id = $product_data['product_id'];
				$p_all_price_in_huf = $product_data['price_in_huf'];
				$p_quantity = (int) $product_data['quantity'];
				$p_megapack = $product->get_product_attr_by_id('megapack', $p_id);


				if ( $p_megapack === TRUE ) {
					if ( $p_quantity >= 12 ) {

						$number_of_amount_deduction_product = (int) floor($p_quantity / 12);


						if ( $number_of_amount_deduction_product > 0 ) {
							$discount_price = - ($number_of_amount_deduction_product * 6000);

							$basket->change_basket_product_discount_price($p_id, $discount_price);
							$basket->change_basket_product_discount_quantity($p_id, $discount_quantity);
							$basket->change_basket_product_discount_name($p_id, $discount_name);
						}
					}
				}
			}
		}
	}


	// Get Discount Price
	public function get_product_discount_price($product_id = "") {
		$basket = new Basket;
		$basket_content = $basket->get_basket();

		if ( !empty($basket_content) && is_array($basket_content) ) {
			foreach ($basket_content as $product_data) {
				if ( $product_data['product_id'] == $product_id ) {
					if ( isset($basket_content[$product_id]['discount_data']['price_in_huf']) ) {
						return $basket_content[$product_id]['discount_data']['price_in_huf'];
					}
				}
			}
		}

		return FALSE;
	}


	// Get Discount Quantity
	public function get_product_discount_quantity($product_id = "") {
		$basket = new Basket;
		$basket_content = $basket->get_basket();

		if ( !empty($basket_content) && is_array($basket_content) ) {
			foreach ($basket_content as $product_data) {
				if ( $product_data['product_id'] == $product_id ) {
					if ( isset($basket_content[$product_id]['discount_data']['quantity']) ) {
						return $basket_content[$product_id]['discount_data']['quantity'];
					}
				}
			}
		}

		return FALSE;
	}


	// Get Discount Name
	public function get_product_discount_name($product_id = "") {
		$basket = new Basket;
		$basket_content = $basket->get_basket();

		if ( !empty($basket_content) && is_array($basket_content) ) {
			foreach ($basket_content as $product_data) {
				if ( $product_data['product_id'] == $product_id ) {
					if ( isset($basket_content[$product_id]['discount_data']['name']) ) {
						return $basket_content[$product_id]['discount_data']['name'];
					}
				}
			}
		}

		return FALSE;
	}


	// Get Discount Type
	public function get_product_discount_type($product_id = "") {
		$basket = new Basket;
		$basket_content = $basket->get_basket();

		if ( !empty($basket_content) && is_array($basket_content) ) {
			foreach ($basket_content as $product_data) {
				if ( $product_data['product_id'] == $product_id ) {
					if ( isset($basket_content[$product_id]['discount_data']['name']) ) {
						return $basket_content[$product_id]['discount_data']['name'];
					}
				}
			}
		}

		return FALSE;
	}

}