<?php 

class Product extends Functions {

	public $products = array();



	public function get_products() {

		$products[] = array(
							'name' => 'téliszalámi',
							'price' => 2000,
							'megapack' => FALSE
						);

		$products[] = array(
							'name' => 'gumikacsa',
							'price' => 3000,
							'megapack' => FALSE
						);

		$products[] = array(
							'name' => 'megapack uborka',
							'price' => 2800,
							'megapack' => TRUE
						);

		$products[] = array(
							'name' => 'megapack gesztenye',
							'price' => 1000,
							'megapack' => TRUE
						);

		return $products;
	}


	// Get product attr by ID: name || price || ...
	public function get_product_attr_by_id($attr_name = "", $product_id = "") {
		$products = $this->get_products();

		if ( isset($products[$product_id][$attr_name]) ) {
			return $products[$product_id][$attr_name];
		}

		return FALSE;
	}


}