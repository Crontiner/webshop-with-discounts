<?php
session_start();

define('DS', DIRECTORY_SEPARATOR);
define('SITE_ROOT', str_replace(DS.'includes', "", __DIR__));
define('INCLUDES_PATH', SITE_ROOT.DS.'includes');

require_once(INCLUDES_PATH.DS."Functions.php");
require_once(INCLUDES_PATH.DS."Basket.php");
require_once(INCLUDES_PATH.DS."Product.php");
require_once(INCLUDES_PATH.DS."Discount.php");