<?php

class Basket extends Functions {
	public $basket = array();


	public function set_product_to_basket($product_data) {

		if ( !empty($product_data) ) {

			// Set products base price
			$p = new Product;
			$p_price_in_huf = $p->get_product_attr_by_id('price', $product_data['product_id']);
			
			if ( $p_price_in_huf !== FALSE ) {
				$product_data['price_in_huf'] = $p_price_in_huf * $product_data['quantity'];
			} else {
				$product_data = "";
			}


			if ( !empty($product_data) ) {
				$this->basket[] = $product_data;
				$_SESSION['basket'] = $this->basket;

				return TRUE;
			}
		}

		return FALSE;
	}

	public function get_basket() {
		return $_SESSION['basket'];
		//return $this->basket;
	}


	public function delete_basket() {
		if ( isset($_SESSION['basket']) ) {
			unset($_SESSION['basket']);

			return TRUE;
		}
		return FALSE;
	}


	public function change_basket_product_discount_price($product_id = "", $discount_price = 0) {

		if (isset($_SESSION['basket']) && !empty($_SESSION['basket'])) {
			foreach ($_SESSION['basket'] as $key => $product_data) {

				if ( $product_data['product_id'] == $product_id ) {
					$_SESSION['basket'][$key]['discount_data']['price_in_huf'] = (float) $discount_price;

					return TRUE;
				}
			}
		}

		return FALSE;
	}


	public function change_basket_product_discount_quantity($product_id = "", $discount_quantity = 0) {

		if (isset($_SESSION['basket']) && !empty($_SESSION['basket'])) {
			foreach ($_SESSION['basket'] as $key => $product_data) {

				if ( $product_data['product_id'] == $product_id ) {
					$_SESSION['basket'][$key]['discount_data']['quantity'] = (int) $discount_quantity;

					return TRUE;
				}
			}
		}

		return FALSE;
	}


	public function change_basket_product_discount_name($product_id = "", $discount_name = "") {

		if (isset($_SESSION['basket']) && !empty($_SESSION['basket'])) {
			foreach ($_SESSION['basket'] as $key => $product_data) {

				if ( $product_data['product_id'] == $product_id ) {
					$_SESSION['basket'][$key]['discount_data']['name'] = $discount_name;

					return TRUE;
				}
			}
		}

		return FALSE;
	}

}
